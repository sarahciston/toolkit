# Workshop zines &#128424;

[//]: # "ANCHOR: zinelist"

* &#128175; [Intersectional Data Bodies](./Zines.md#databodies) created at Halfsister Gallery Jan 2023
* &#127744; [Intersectional AI Anarchies](./Zines.md#anarchies) created at Akademie der Künste Oct 2022
* &#x1F49C; [An AI Love Letter from Berlin](assets/pdf/digital/IAITzine-digital-berlin.pdf) created at the [HIIG IAI Edit-a-thon](./Community.md#Events) Sep 2021
* &#x1F60D; [Help Me Code IAI](./Code.md) created at the [Creative Code Collective](https://creativecodecollective.com) USC Zine-Making Workshop Nov 2021
*  &#128681; [What Shouldn't AI Be Used For?](assets/pdf/digital/IAITzine-digital-shouldnt.pdf) created at Mozilla Festival online Mar 2022
*  &#128302; [Intersectional AI Futures](./Zines.md#futures) created at USC in Los Angeles May 2022
*  &#128396; [How Can Artists Help Reshape AI?](./Zines.md#artists) created at ZK/U in Berlin Sep 2022
*  &#128587; [Hi AI, We Have Questions](./Zines.md#questions) created at ZK/U in Berlin Sep 2022

[//]: # "ANCHOR_END: zinelist"

[//]: # "ANCHOR: makezine"

### &#128526; How do I make my own zine?

!["fold at solid lines, cut at center scored lines only"](assets/img/zineHowTo.png "fold at solid lines, cut at center scored lines only")!["refold and pinch toward center"](assets/img/zineHowTo2.png)
<!-- {: .img-small} -->

1.  fold your paper in half long ways, then unfold. 
2.  fold your paper in half short ways, then unfold. 
3.  fold the edges of your paper toward the middle, and unfold. you should have eight mini sections. 
4.  cut ONLY along the two short folds in the middle, by folding the paper in half short ways again. find the folded edge and cut only halfway in (not from the side that is open). do not cut all the way across. unfold. the goal is to have a slice in the center that does not connect to any edges. 
5.  finally, fold the paper in half long ways again so the printed side faces out. pinch open the sliced center and separate those pages apart from each other until they join their neighbors. 
6.  fold the book closed with the covers on the front and back.

[//]: # "ANCHOR_END: makezine"

<!-- {{#include Glossaries.md:zines}} [Glossaries &#127846;&#127846;](./Glossaries.md#zines) -->

<!-- width="350px" height="600px"  -->

----

[//]: # "ANCHOR: databodieszine"

<div markdown="1" style="float:left;margin-left:15px;" id="databodies">

## Intersectional Data Bodies

<object markdown="1" type="application/pdf" width="700px" height="800px" data="assets/pdf/digital/IAITzine-digital-databodies.pdf">[Help Me Code IAI Zine PDF](assets/pdf/digital/IAITzine-artists-databodies.pdf)</object>

[Print & Fold Edition: A3/Tabloid](assets/pdf/print/IAITzine-a3-databodies.pdf)

</div>

[//]: # "ANCHOR_END: databodieszine"


[//]: # "ANCHOR: anarchieszine"

<div markdown="1" style="float:left;margin-left:15px;" id="anarchies">

## AI Anarchies Intersectional AI Zine

<object markdown="1" type="application/pdf" width="700px" height="800px" data="assets/pdf/digital/IAITzine-digital-anarchies.pdf">[AI Anarchies Autumn School Workshop Intersectional AI Zine PDF](assets/pdf/digital/IAITzine-digital-anarchies.pdf)</object>

[Print & Fold Edition: A3/Tabloid](assets/pdf/print/IAITzine-a3-anarchies.pdf)

</div>

[//]: # "ANCHOR_END: anarchieszine"

[//]: # "ANCHOR: berlinzine"

<div markdown="1" style="float:left;margin-right:15px;" id="berlin">
<!-- style="float:right;margin-left:15px;"  -->

## An AI Love Letter from Berlin
<!-- width="700px" height="800px" -->
<!-- width="350px" height="600px" -->
<object markdown="1" type="application/pdf" width="700px" height="800px" data="assets/pdf/digital/IAITzine-digital-berlin.pdf">[Help Me Code IAI Zine PDF](assets/pdf/digital/IAITzine-digital-berlin.pdf)</object>

[Print & Fold Edition: A3/Tabloid](assets/pdf/print/IAITzine-a3-berlin.pdf)

</div>

[//]: # "ANCHOR_END: berlinzine"

[//]: # "ANCHOR: codezine"

<div markdown="1" style="float:left;margin-left:15px;" id="code">

## Help Me Code IAI

<object markdown="1" type="application/pdf" width="700px" height="800px" data="assets/pdf/digital/IAITzine-digital-code.pdf">[Help Me Code IAI Zine PDF](assets/pdf/digital/IAITzine-digital-code.pdf)</object>

[Print & Fold Edition: A3/Tabloid](assets/pdf/print/IAITzine-a3-code.pdf)

</div>

[//]: # "ANCHOR_END: codezine"

[//]: # "ANCHOR: shouldntzine"

<div markdown="1" style="float:left;margin-left:15px;" id="shouldnt">

## What Shouldn't AI Be Used For?

<object markdown="1" type="application/pdf" width="700px" height="800px" data="assets/pdf/digital/IAITzine-digital-shouldnt.pdf">[Help Me Code IAI Zine PDF](assets/pdf/digital/IAITzine-digital-shouldnt.pdf)</object>

[Print & Fold Edition: A3/Tabloid](assets/pdf/print/IAITzine-a3-shouldnt.pdf)

</div>

[//]: # "ANCHOR: artistszine"

<div markdown="1" style="float:left;margin-left:15px;" id="artists">

## How Can Artists Help Reshape AI?

<object markdown="1" type="application/pdf" width="700px" height="800px" data="assets/pdf/digital/IAITzine-digital-artists.pdf">[Help Me Code IAI Zine PDF](assets/pdf/digital/IAITzine-digital-artists.pdf)</object>

[Print & Fold Edition: A3/Tabloid](assets/pdf/print/IAITzine-a3-artists.pdf)

</div>

[//]: # "ANCHOR_END: artistszine"

[//]: # "ANCHOR: futureszine"

<div markdown="1" style="float:left;margin-left:15px;" id="futures">

## Intersectional AI Futures

<object markdown="1" type="application/pdf" width="700px" height="800px" data="assets/pdf/digital/IAITzine-digital-futures.pdf">[Help Me Code IAI Zine PDF](assets/pdf/digital/IAITzine-digital-futures.pdf)</object>

[Print & Fold Edition: A3/Tabloid](assets/pdf/print/IAITzine-a3-futures.pdf)

</div>

[//]: # "ANCHOR_END: futureszine"

[//]: # "ANCHOR: questionszine"

<div markdown="1" style="float:left;margin-left:15px;" id="questions">

## Hi AI, We Have Questions

<object markdown="1" type="application/pdf" width="700px" height="800px" data="assets/pdf/digital/IAITzine-digital-questions.pdf">[Help Me Code IAI Zine PDF](assets/pdf/digital/IAITzine-artists-questions.pdf)</object>

[Print & Fold Edition: A3/Tabloid](assets/pdf/print/IAITzine-a3-questions.pdf)

</div>

[//]: # "ANCHOR_END: questionszine"

