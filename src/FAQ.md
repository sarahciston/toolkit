# Intersectional AI FAQs

## We have questions

<div style="float:right;margin-left:15px;">
<object markdown=1 type="application/pdf" width="350px" height="600px" data="assets/pdf/digital/IAITzine-digital-questions.pdf">[Help Me Code Intersectional AI Zine PDF](assets/pdf/digital/IAITzine-digital-questions.pdf)</object>

[Print & Fold Edition: A3/Tabloid](assets/pdf/print/IAITzine-a3-questions.pdf)

</div>

-  *Should I be more careful with my data?*

-  *How is AI powered?*

-  *What is AI made of?*

-  *What happens when AI makes a mistake?*

-  *What does it mean to learn like a machine?*

-  *How are decisions made in AI systems?*

-  *How does AI mirror society?* 

-  *Who does AI exclude?*

## Foundational concepts of AI systems explained

<!-- some of this would go in a theory section? -->


>"there is no "neutral," "natural," or "apolitical" vantage point that training data can be built upon. There is no easy technical "fix" by shifting demographics, deleting offensive terms, or seeking equal representation by skin tone. The whole endeavor of collecting images, categorizing them, and labeling them is itself a form of politics, filled with questions about who gets to decide what images mean and what kinds of social and political work those representations perform." [^Excavating]

[^Excavating]: Crawford, K., & Paglen, T. (2019). Excavating AI: The Politics of Images in Machine Learning Training Sets. 2019, September 19. https://www.excavating.ai


#### WHY DOES TECH GET AWAY WITH UNSUSTAINABLE BEHAVIOR? 

Why do current efforts toward bias mitigation and diversity fail?
1. not seen as a socio-technical system: explainability, critical code studies
2. not seen as being about power: interesectionality, who has power, not just bias
3. not seen as a multiplicity: decolonisation, whose morals, whose benefit


To not think to train on a variety of faces, or to include an operation in the code to account for language differences, happens because there is not a variety of perspectives in the room when these very human decisions are being made before, during, and after the data is being collected, before, during, and after the code is being written and run. 


<!-- fundamental technical principles of ML through Int lenses, unpacked for broader audiences-->

### Machine conditioning, not machine learning

- "Machine conditioning" not machine learning: "the fuzzy usage of ‘learning’ anthropomorphises a mathematical optimisation process" [@gaitelyriqueFrancisHungerUnHype2021]  or "interpretation machines" [@lohmannInterpretationMachinesContradictions2020]

### Simplistic non-understanding
  - "Given the statistical distribution of words in the vast public corpus of (English) text, what words are most likely to follow the sequence [...]? (Murray Shanahan (2022), "Talking About Large Language Models" @shanahanTalkingLargeLanguage2023)

### Metaphors, not brains

- Replace neural metaphors: "Neurons" in neural networks are pre-set nodes that determine the weights and probabilities of information, ie *they do math.* The "brain" structure is a metaphor, what are other metaphors?

### Activation & loss

The nodes of neural networks are simply functions, or mathematical operations that take inputs and process them according to a formula. What determines those functions has become standardized over decades of experimentation in machine learning research. Functions at different points in the network serve different purposes. 

Activation functions determine which nodes are "activated" and permitted to pass their information on to the next nodes. Standard activation functions like [XXX "Soft-Max" and RELU] determine this by normalizing the numbers to a baseline close to 0 and cutting out any information that falls below a threshold. The shape of the threshold determines how strictly information is filtered out or allowed to pass. 

Loss functions similarly [XXX]

### Vectors & tensors: When you're a computer, everything's a number

### Models & transformers: encoding, decoding
  - ML systems both create and interpret the numerical representations of information. 

### Semi/Un/Supervised depends on human labeling

  - Was labeled, pre-sorted data used to train it? (Supervised) 
  - Or was the data unstructured, like plain text? (Unsupervised)
  - Unsupervised learning means data are even more essential to the models' outputs
  - crowd work! 

#### Scale not complexity

Complexity here is about scale: The speed and quantity of calculations that computation enables, NOT the complexity of those calculations themselves.

### Binaries & Booleans
  - Similarity & Difference
    - Often seen technically as a measure of "distance"
    - But of what "dimension" (aspect) and measured (represented numerically) in what way?
    - cosign similarity between the vectors (a function of the angle between the 2, cosign "normalizes" for scale)
    - What of the varied social understandings of similarity & difference?

### Spatializing metaphors
  - "Grenzprobleme"
    - "Border problems" (via Pedro Oliveira)
    - "Our research shows that the BAMF's language analysis software makes mistakes - just like the people who use it."
    - Categorization is always a question of in/out-side. Concepts are codified through the creation and reproduction of these boundaries.

### Uncertainty reduced to certainty 
  - "[The algorithm] presents something as a singular optimal output, when it is actually generated through multiple and contingent relations. [...] I understand the spatial logic of algorithms to be an arrangement of propositions that significantly generates what matters in the world." (Louise Amoore, Cloud Ethics @amooreCloudEthicsAlgorithms2020)

### Displaced agency, diffused responsibility

### Bias transfer
  - "bias transfer both (a) arises in realistic settings (such as when pre-training on ImageNet or other standard datasets) and (b) can occur even when the target dataset is explicitly de-biased" [@salmanWhenDoesBias2022]




#### Classification logics

- Pervasive capture, data colonialism: 
  - >"the audacious yet largely disguised corporate attempt to incorporate all of life, whether or not conceived by those doing it as “production,” into an expanded process for the generation of surplus value. The extraction of data from bodies, things, and systems create new possibilities for managing everything. This is the new and distinctive role of platforms and other environments of routine data extraction." (Nick Couldry & Ulises A. Mejias  [@couldryMakingDataColonialism2019])
- Garbage in, garbage out: 
  - Conscientious collection methods, values and questions 
  - Proprietary, missing, indecipherable data, practices and documentation
  - Labor:
    - "The data labelers employed by Sama on behalf of OpenAI were paid a take-home wage of between around $1.32 and $2 per hour" [@ExclusiveHourWorkers]
    - "ChatGPT and other generative models are not magic – they rely on massive supply chains of human labor and scraped data, much of which is unattributed and used without consent,” Andrew Strait
    - "Automation runs on the averaged microjudgments of whole detachments of underpaid humans, not some supersmart computer." [Steyerl (2022) @steyerlMeanImages2023]
- Unacknowledged curation: 
  - >"All of these attempts to harmonize and create general descriptive languages are founded on a universalizing logic that in its most utopian dimensions believes that these political and infrastructural differences that stand in the way of classification are accidental and can ultimately be overcome." [Justin Joque (2015) @joqueDataCreationMeaning2015]
- Data bodies, algorithmic race & gender, the problem of representation: 
  - “your algorithmic gender may well contradict your own identity, needs, and values. Google’s gender is a gender of profitable convenience” (John Cheney-Lippold, We Are Data @cheney-lippoldWeAreData2017)
  - whose data are (not) included:
    - "we need to interrogate not only the security of our data but  [...] What kinds of stories cannot be told [...] because individuals do not have access to [a boutique self-tracking device], the time to engage it fully, and/or do not have the security to reveal the details of their everyday experience without increasing their risk of domestic or state violence?" (Jacqueline Wernimont, Numbered Lives [@wernimontNumberedLivesLife2018])
    - whose stories are these? "what subject position allows us to ethically occupy, flourish, and move in a world of pervasive data monitoring? (Natasha Dow Schull)
- Worldbuilding with data: 
  - “Database design, in that way, is 'an exercise in worldbuilding,' a normative process in which programmers are in a position to project their world views—a process that all too often reproduces the technology of race.” [@benjaminRaceTechnologyAbolitionist2019]
- Uncertainty reduced to certainty 
  - "[The algorithm] presents something as a singular optimal output, when it is actually generated through multiple and contingent relations. [...] I understand the spatial logic of algorithms to be an arrangement of propositions that significantly generates what matters in the world." [@amooreCloudEthicsAlgorithms2020]
  - >>"If uncertainty is the ability for the unknown to be accounted for by comporting to containment under the logics of algorithmic models, then what can unknowability offer instead? [...]["BLACKNESS as space of ethical absence," "the unknown as a subject"] Unknowability born from the figure of the flesh opens the space for ways of understanding the world that are otherwise discounted because of their inability to be neatly measured and accounted for. It is the place in which irresolution is allowed to reside." (Morrison 2021, 255) >>"flesh becomes an essential outlier from which to assess the limitations of sociotechnical fixes. Flesh is the poignant, unavoidable signifier and text upon which to read the history and reality of total expropriation of value from bodies and land." (255) >>"leaving open the unresolved space of coming to know something that is relational: taking into consideration the specific context of the speaker, the place from which they speak, the closeness they share, and the mode through which that speaking happens. To know in this context is not a territorial claim to be made, enclosed, and defended but an endured practice of proximity. To speak nearby is a gesture of knowing that requires engagement, perforating the hermetic encapsulation of totality." (253) *relational knowing* [@morrisonFlesh2021]
Morrison, Romi Ron. (2021). "Flesh." In Thylstrup, N. B., Agostinho, D., Ring, A., D’Ignazio, C., & Veel, K. (Eds.) Uncertain Archives: Critical Keywords for Big Data. (pp. 240-257). The MIT Press.  https://doi.org/10.7551/mitpress/12236.003.0027
- Displaced agency, diffused responsibility
- Bias transfer:
  - "bias transfer both (a) arises in realistic settings (such as when pre-training on ImageNet or other standard datasets) and (b) can occur even when the target dataset is explicitly de-biased" @salmanWhenDoesBias2022


>"computers were humans (mostly women), and digital computers were primarily used for complex calculations, especially in wartime military contexts. Amid the ballistic calculations, Turing speculated on a prompt from his teacher, philosopher Ludwig Wittgenstein: Can machines think? Both men thought it was a ridiculous question" [@veeTextGenEdTeachingText2023]