# Intersectionality

## What Intersectionality Means to Me 

### And me, and me, and me too 

### Bringing the marginalized back to the center 

### I want to get it but ... 

![Intersectionality Made Easy](assets/img/EasyIntersect.gif)[^1]

### If you're wondering, "Why should I have to get it?" 

![Audre Lorde](assets/img/AudreLordeSingleIssue.jpg)

#### Questions to answer in this section: 

![AI To-Do List\|400px\|right](assets/img/LC-AIToDoList.png)

-   Why listen to academic, activist, or artistic approaches?
-   Why take a specifically intersectional approach?
-   Why does considering multiple perspectives and different identities matter so much for thinking about technology?
-   Why attach such convoluted-seeming phrases to these ideas, and what do they even mean?
-   Why a print zine or a digital platform where anyone can contribute?
-   Whose responsibility is it to make the effort to learn, to try harder? (The difference between barriers to access and privileged discomfort.)

## Intersectionality for Engineers?

#### Intersectionality as a weighted network

A person moving through a system
All the many factors that influence how much friction they have moving through the system or how much ease. Highly weighted data move easily and are pushed forward. Low weighted data may never make it through or have to have extremely strong aspects that offset the negative weights. These weights are determined from outside, not by the datapoints themselves. They are set to keep the system working as it does.

In complex systems multiple variables interact. These create unique and complicated experiences that are impossible to quantify at an individual level. 

Considering how power (weights) work in any system, what the system "sees" and misses, can help you think beyond simplistic modeling when you're designing a system or think beyond labels when you are moving through the world. 

Is this a helpful metaphor or does it reinforce zero-sum thinking? 

## How can artists contribute? 

[//]: # "Maybe this goes in the intro?"

>"Many artists sought to provoke audiences by defamiliarizing the opaque and proprietary nature of software tools commonly covered by trade secret protections and functioning as “black boxes.” [...] Other artists described deploying defamiliarization to mobilize audience emotions as an explicitly ethical act."(Stark & Crawford 2019, 446)
>"Many artists also noted concrete plans for educating and contextualizing were vital to the ethics of their art practice." (Stark & Crawford 2019, 448)

[^Stark]: Stark, L., & Crawford, K. (2019). The Work of Art in the Age of Artificial Intelligence: What Artists Can Teach Us About the Ethics of Data Practice. Surveillance & Society, 17(3/4), 442–455. https://doi.org/10.24908/ss.v17i3/4.10821

"Value Sensitive Design (VSD) methodology, which aims to contribute to both the critical analysis of (dis)values in existing technologies and the construction of novel technologies that account for specific desired values. " Policy Review


</section>

<section class="zine-page page-3" markdown="1">

### Avoiding purplewashing, greenwashing, and disingenuous cause-claiming 

<div class="small float-left mr-4">

![Intersectionality Made Easy](../assets/img/EasyIntersect.gif)

[Laci Green](https://twitter.com/gogreen18) & [Francesca Ramsey](https://twitter.com/chescaleigh)
{: .small .float-left .mr-4 }

</div>

</section>

<section class="zine-page page-4" markdown="1">
</section>

<section class="zine-page page-5" markdown="1">
</section>

<section class="zine-page page-6" markdown="1">
</section>

<section class="zine-page page-7" markdown="1">
</section>

<section class="zine-page page-8" markdown="1">
</section>
</main>

<!-- references -->