# &#x1F49C; Love Notes to Intersectional AI

Many, many people believe better AI futures are possible. We hope you draw inspiration for IAI from a wide range of practitioners already in action! Here is an ever-growing list of projects, tools, resources to learn from. Some of these are AI– or tech-adjacent, but we think all of them are cool ways to get into thinking about tech differently.

[Creative Code Collective Resource Hub](https://creativecodecollective.github.io/resource-hub/) is an interactive, sortable version of this effort (work in progress). Please add any and all items listed here to the hub: [Share via this form](https://forms.gle/1ffhL7yAdWP6jC5Z6). 

<div class="warning">

*Wishlist...*

* More international examples: would love to know more from outside US & EU!
* Local and in-person examples from where you live

</div>

<!-- <div markdown="1" style="float:right;margin-left:15px;">
<object markdown="1" type="application/pdf" width="350px" height="600px" data="assets/pdf/digital/IAITzine-digital-berlin.pdf">[AI Love Letter from Berlin Intersectional AI Zine PDF](assets/pdf/digital/IAITzine-digital-berlin.pdf)</object>

[Print & Fold Edition: A3/Tabloid](assets/pdf/print/IAITzine-a3-berlin.pdf)

</div> -->

## Intersectional Tools & Resources

### Approaches & Frameworks

- [Not My AI](https://notmy.ai/)
- [Indigenous AI](https://www.indigenous-ai.net/)
- [AI Decolonial Manyfesto](https://manyfesto.ai/)
- [If AI, Then Feminist](https://netzforma.org/publikation-wenn-ki-dann-feministisch-impulse-aus-wissenschaft-und-aktivismus)
- [Indigenous AI](https://www.indigenous-ai.net/)
- [Queer.AI](https://queer.ai/) \"conversational agents for the advancement of new eroticisms
- [Guidelines for Checking Essential Properties of AI-Based Systems](https://algorithmwatch.org/de/auto-hr/leitfaden/) in German
- [Feminist Tech Policy](https://superrr.net/feministtech/)by superrr
- [Dreaming Beyond AI](https://dreamingbeyond.ai/en) "a space for critical and constructive knowledge, visionary fiction & speculative art and community-organising"
- [AIxDesign](https://aixdesign.co/) "nourishing alternative, feminist, + participatory approaches to ai for the rest of us"
- [Katrin Fritsch: Towards an Emancipatory Understanding of Widespread Datification](https://medium.com/@katrinfritsch/towards-an-emancipatory-understanding-of-widespread-datafication-5a53ed79eb0f)
- [Feminist Internet](https://www.feministinternet.com/) \"There is no feminism, only possible feminisms. There is no internet, only possible internets."
- [Algo.Rules](https://algorules.org/en/home)
- [Civic Data Library of Context](https://www.civicdatalibrary.org/)
- [Afrotechtopia](https://www.afrotectopia.org/)
- [Data for Black lives](https://d4bl.org/)
- [MOTIF feminist futures](https://feministfutures.net/)
- [Superrr](https://superrr.net/) "building diverse & equal futures in tech and beyond"
- [Data Justice Lab](https://datajusticelab.org/) 
- [Our Data, Ourselves](https://ourdataourselves.tacticaltech.org/) and the works of [Tactical Tech](https://tacticaltech.org/projects)
- [Interspecies Internet](https://www.interspecies.io/about)
- [Fix the Glitch Toolkit 2.0](https://glitchcharity.co.uk/wp-content/uploads/2021/04/Glitch-Toolkit-FULL-Interactive.pdf) "Helping to End Online Gender Based Violence for Black Women\"
- [Framework for Participatory Data Stewardship](https://www.adalovelaceinstitute.org/report/participatory-data-stewardship/)

### Tutorials

- [Elements of AI](https://course.elementsofai.com/)
- [fast.ai course](https://course.fast.ai/videos/?lesson=1) Practical Deep Learning for Coders, Code-First Intro to NLP, Practical Data Ethics
- [Why.AI](https://www.hiig.de/en/dossier/why-ai/) dossier debunking myths in plain language
- [Dive Into Deep Learning](https://d2l.ai/) open source book, technical
- [Generative Engine](https://experiments.runwayml.com/generative_engine/) exploration of VQGAN-CLIP
- [\@TheAnnaLytical](http://instagram.com/theannalytical) [Anna Lytical Tutorial](https://youtu.be/SpzN47A7gqg) Glamorous Javascript: Makeup and Coding Edition #tutorial
<!-- - [DIY AI: ML5 Community Toolkit](https://ml5toolkit.ml/) -->

### Projects & Examples

-   [Drag Deep Fakes](https://www.youtube.com/watch?v=qQSSl533rb8) Screen Walks. (2021, April 7) <https://www.youtube.com/watch?v=qQSSl533rb8> #vision #deepfake #gan
-   [Library of Missing Datasets](https://github.com/MimiOnuoha/missing-datasets) #datacollection by Mimi Onuoha
-   [Glaze Project](https://glaze.cs.uchicago.edu/) disguise your artwork
-   [Syb Trans Voice Interface](http://syb.feministchatbot.com/) 
-   [Feminist Guide to AI Bias (chatbot)](http://about.f-xa.co/2/)
-   [Designing Feminist Chatbots](https://drive.google.com/file/d/0B036SlUSi-z4UkkzYUVGTGdocXc/view?resourcekey=0-DS-Lj4uCk2VHf1cuogGNfg) by Josie Swords @swordstoyoung

### Teaching

-   [Critical Coding Cookbook](https://parsonsdt.github.io/critical-coding-cookbook/)
-   [DecarceratingTheClassroom](https://decarceratingtheclassroom.myportfolio.com/archive)
-   [TextGenEd](https://wac.colostate.edu/repository/collections/textgened/) Book of prompts for teaching critically with text-generating AI, edited by Annette Vee, Tim Laquintano, Carly Schnitzler

### Communities

- [Feminist.AI](https://feminist.ai)
- [Color Coded LA](https://colorcoded.la)
- [JUST AI](https://www.adalovelaceinstitute.org/just-ai/)
- [Coding Rights](http://codingrights.org/)
- [Black in AI](https://blackinai.github.io/)
- [Virtual Care Lab](https://virtualcarelab.com/)
- [School for Poetic Computation](https://sfpc.io/)
- [Lurk](https://lurk.org/)

### Local: Berlin 

- [CreativeCode.Berlin](https://github.com/CreativeCodeBerlin/creative-coding-minilist)
- [School of Machines, Making, & Make-Believe](http://schoolofma.org/) classes on art, tech, & design

### Tools

- [Mukurtu CMS](https://mukurtu-australia-nsw.libraries.wsu.edu/mukurtu-cms) data platform for Indigenous communities to share and protect cultural heritage
- [Switching.Software](https://switching.software/list/all-in-one-services/) #deplatforming
- [Anti Capitalist Software License](https://anticapitalist.software/)
- [In Solidarity](https://github.com/marketplace/in-solidarity)
- [Threads](https://cyber.harvard.edu/projects/threads) anonymity in forums tool by Berkman Klein
- [Tiny Tools Directory](https://tinytools.directory/) gathers open source tools, small, free or experimental #compendium #collection #tool
- [Read the Feminist Manual](https://psaroskalazines.gr/zines/RTFM/) chrome add-on to swap pronouns to gender-neutral

### Zines, Principles, Readings, and...

- [Internet Teapot](https://internetteapot.com/) and [Algorithms of Late Capitalism Zines](https://algorithmsoflatecapitalism.tumblr.com/zines), esp. special edition: [Reconstructing AI](https://href.li/?https://firebasestorage.googleapis.com/v0/b/internet-teapot.appspot.com/o/ALC%20-%20Dreams%20of%20Visionary%20Fiction.pdf?alt=media&token=4c3a9d31-922d-4dbe-9c6f-42fbd69fe312)
- [Oracle for Transfeminist Technologies](https://www.transfeministech.codingrights.org/) by [Coding Rights\'](http://codingrights.org/) Joana Varon and Clara Juliano
- [A is for Another: A Dictionary of AI](https://aisforanother.net/pages/site.html)
- [A New AI Lexicon](https://medium.com/a-new-ai-lexicon/) from AI Now Institute, specifically the first sequence of CARE
- [Detroit Digital Justice Coalition Principles](http://detroitdjc.org/principles/) Access, Participation, Common Ownership, Healthy Communities
- [Tiny Tech Zines](https://tinytechzines.org/)
- [Our Data Bodies' Digital Defense Playbook](https://www.odbproject.org/tools/)
- [Feminist Data Set & Toolkit](https://carolinesinders.com/feminist-data-set/) Caroline Sinders
- [How to Write Non-Violent Creative Code](https://contributors-zine.p5js.org/#reflection-olivia-mckayla-ross)
- [We Need to Talk AI: A Comic Essay](https://weneedtotalk.ai/)
- [Feminist Principles of the Internet](https://feministinternet.org/)
- [Feminist Data Manifest-NO](https://www.manifestno.com/)
- [Techno-Galactic Guide to Software Observation](https://monoskop.org/images/e/e3/The_Techno-Galactic_Guide_to_Software_Observation_2018.pdf)
- [Women in Computation, Portable Syllabus](https://sarahciston.github.io/intersectionalai/assets/files/WomenInComputation_PortableSyllabus.pdf)
- [A People's Guide to Artificial Intelligence](https://alliedmedia.org/wp-content/uploads/2020/09/peoples-guide-ai.pdf), Mimi Onuoha and Mother Cyborg (a.k.a. Daina Nucera)
- [coveillance toolkit](https://coveillance.org/)
- [Networks of One's Own](https://networksofonesown.constantvzw.org/etherbox/manual.html) publishing an issue as a hardware-software stack with feminist server principles etc. [Issue 2, NoOO: Three Takes on Taking Care](https://networksofonesown.vvvvvvaria.org/)
- [Feminist Infrastructure Zine](https://alexandria.anarchaserver.org/index.php/Feminist_Infrastructure) in English and en Español
- [Reclaiming Digital Infrastructures Zine](https://constantvzw.org/documents/RDI/Reclaiming%20Digital%20Infrastructures.pdf)

#### Web/print tools

- [Bindery Javascript Library](https://bindery.info) or [Paged.js](https://pagedjs.org/documentation/2-getting-started-with-paged.js/) or [html2print by OSP](http://osp.kitchen/tools/html2print/)
- [Markdown Cheatsheet](https://devhints.io/markdown)
- [ZineMachine](https://zine-machine.glitch.me/)
- [P5.js](http://p5js.org)
- [p5.js Contributors Zine](https://contributors-zine.p5js.org/#read)
- [Decent Patterns](https://decentpatterns.xyz/library/) "Decentralization Off the Shelf" (DOTS) offers design patterns for rethinking user applications
- [In Solidarity](https://github.com/marketplace/in-solidarity) github app for ensuring more inclusive language in technical documents and code
- [How to make a zine](https://thecreativeindependent.com/guides/how-to-make-a-zine/)

### Beloved readings

-   [Ahmed, Sara. Feminist Killjoy Survival Kit.](https://www.saranahmed.com/thefeministkilljoyhandbookandacomplainershandbook)
-   [brown, adrienne maree. Emergent Strategies.](https://adriennemareebrown.net/book/emergent-strategy/)
-   [Nakamura, Lisa. 2014. "Indigenous Circuits: Navajo Women and the Racialization of Early Electronic Life." The American Studies Association.](https://computerhistory.org/blog/indigenous-circuits/)
    <!-- >"Indian-identified traits and practices such as painstaking attention to craft and an affinity for metalwork and textiles were deployed to position the Navajo on the cutting edge of a technological moment precisely because of their possession of a racialized set of creative cultural skills in traditional, premodern artisanal handwork." -->

#### Historical zines of interest & related reading 

- [Cyberfeminist Index](https://cyberfeminismindex.com/) #cyberfeminism #compendium #collection
- [Radical Software](https://radicalsoftware.org/e/index.html)
- [People's Computer Company Newsletter](https://archive.org/embed/1972PccV1N1)
- [Computer Lib Dream Machines](https://archive.org/embed/computer-lib-dream-machines)
- [2600 Hacker Magazine](https://archive.org/details/2600magazine/2600_1-10/)
- [*Information Activism: A Queer History of Lesbian Media Technologies*, by Cait McKinney](http://caitmckinney.com/)
- [Whole Earth Index](https://wholeearth.info/) "access to tools, ideas, and practices," an archive of Whole Earth Catalogs and offshoots between 1970–2002, including the *Whole Earth Software Review* and other couunterculture takes on technology 
- [Homebrew Server Club](https://homebrewserver.club/)


{{#include Zines.md:berlinzine}}
