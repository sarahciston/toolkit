# Datasets &#128175; & Data Bodies

<!-- date: 2022-01-08 -->
<!-- desc: Feed your machines -->

<!-- TK: ADD LIBRARY LISTS -->

[A Critical Field Guide to Working with Machine Learning Datasets](https://knowingmachines.org/critical-field-guide) is now available in collaboration with the Knowing Machines research project. The field guide aims to help you practically navigate the complexities of datasets, and explore the implications of what you choose, build, and design. It invites you to mess with these messy forms and to approach any logic of classification with a critical eye.

## Data Bodies

<div style="float:right;margin-left:15px;">

<object markdown="1" type="application/pdf" width="350px" height="600px" data="assets/pdf/digital/IAITzine-digital-databodies.pdf">[Data Bodies Intersectional AI Zine PDF](assets/pdf/digital/IAITzine-digital-databodies.pdf)</object>

[Print & Fold Edition: A3/Tabloid](assets/pdf/print/IAITzine-a3-databodies.pdf)

</div>


- Data bodies, algorithmic race & gender, the problem of representation: 
  - “your algorithmic gender may well contradict your own identity, needs, and values. Google’s gender is a gender of profitable convenience” (John Cheney-Lippold, We Are Data @cheney-lippoldWeAreData2017)
  - whose data are (not) included:
    - “we need to interrogate not only the security of our data but  [...] What kinds of stories cannot be told [...] because individuals do not have access to [a boutique self-tracking device], the time to engage it fully, and/or do not have the security to reveal the details of their everyday experience without increasing their risk of domestic or state violence?” (Jacqueline Wernimont, Numbered Lives @wernimontNumberedLivesLife2018)
    - whose stories are these? "what subject position allows us to ethically occupy, flourish, and move in a world of pervasive data monitoring? (Natasha Dow Schull)
- Worldbuilding with data: 
  - “Database design, in that way, is 'an exercise in worldbuilding,' a normative process in which programmers are in a position to project their world views—a process that all too often reproduces the technology of race.” (Ruja Benjamin, Race After Technology @benjaminRaceTechnologyAbolitionist2019)

- Pervasive capture, data colonialism: 
  - >"the audacious yet largely disguised corporate attempt to incorporate all of life, whether or not conceived by those doing it as “production,” into an expanded process for the generation of surplus value. The extraction of data from bodies, things, and systems create new possibilities for managing everything. This is the new and distinctive role of platforms and other environments of routine data extraction." (Nick Couldry & Ulises A. Mejias  [@couldryMakingDataColonialism2019])
- Garbage in, garbage out: 
  - Conscientious collection methods, values and questions 
  - Proprietary, missing, indecipherable data, practices and documentation
  - Labor:
    - "The data labelers employed by Sama on behalf of OpenAI were paid a take-home wage of between around $1.32 and $2 per hour" [@ExclusiveHourWorkers]
    - "ChatGPT and other generative models are not magic – they rely on massive supply chains of human labor and scraped data, much of which is unattributed and used without consent,” Andrew Strait
    - "Automation runs on the averaged microjudgments of whole detachments of underpaid humans, not some supersmart computer." [Steyerl (2022) @steyerlMeanImages2023]
- Unacknowledged curation: 
  - >"All of these attempts to harmonize and create general descriptive languages are founded on a universalizing logic that in its most utopian dimensions believes that these political and infrastructural differences that stand in the way of classification are accidental and can ultimately be overcome." [Justin Joque (2015) @joqueDataCreationMeaning2015]


## Datasets and Models, Ingredients and Recipes

It's not just a list of random choices and tools, it's a critical consideration... our choices matter for what we make.

>>"All data are local. Indeed, data are cultural artifacts created by people, and their dutiful machines, at a time, in a place, and with the instruments at hand for audiences that are conditioned to receive them." [...] "[We must learn] to analyze data settings rather than data sets." (Loukissas)

>> Existing datasets perpetuate under-representation and "a range of harmful and problematic representation." They "use cheap tricks", "make ethically dubious questions seem answerable", and "strips away context" (Paulladua et al. 2020) 
<!--"failure to recognize annotation work as *interpretive work*". -->

## Data: Every Input Was Someone Else's Output

Note: This curation of datasets is a sketch in progress being continually updated, and will include a discussion of what makes a more ethical, more critical dataset--is such a thing possible?

### Multimodal (text, image)
* [WIT, Wikipedia-based Image Text Dataset](https://github.com/google-research-datasets/wit), Google's open-source multimodal scraping of Wikipedia
* [Conceptual Captions]()
* [DBpedia](http://wiki.dbpedia.org/) Towards a Public Data Infrastructure for a Large, Multilingual, Semantic Knowledge Graph


### Image Data
* [deepDataset Firefox extension](https://addons.mozilla.org/en-US/firefox/addon/deepdataset/), build your own
* ~~[COCO] Microsoft~~
* ~~[ImageNet] built on WordNet, Stanford Vision Lab~~
  * <!-- >"Whether acknowledged or not, a broad range of participants play an important role in producing the data that is used to train and evaluate ML models. For example, ImageNet, which laid the foundations for deep learning and most image recognition applications and is still used for ML benchmarking, is a dataset of millions of images, taken by hundreds of thousands of people, scraped from the open web and labeled by mTurk workers. Image classification tools are often built on top of models trained on the ImageNet dataset." [@sloaneParticipationNotDesign2022] -->

* ~~[Open Images] Google~~
* [Better Images of AI](https://betterimagesofai.org/), stock images of metaphors for AI
* [ArtEmis: Affective Language for Visual Art](https://www.artemisdataset.org/) A novel large-scale dataset and accompanying machine learning models aimed at providing a detailed understanding of the interplay between visual content, its emotional effect, and explanations for the latter in language"
* [PASS](https://www.robots.ox.ac.uk/~vgg/data/pass/): An ImageNet replacement for self-supervised pretraining without humans. "PASS is a large-scale image dataset that does not include any humans and which can be used for high-quality pretraining while significantly reducing privacy concerns."

### Text Data
* [CommonCrawl](https://commoncrawl.org/the-data/) ?
* [C4: Colossal Cleaned Crawled Corpus](), see [^T5] below
* [Newsroom](https://github.com/lil-lab/newsroom) (https://paperswithcode.com/paper/newsroom-a-dataset-of-13-million-summaries), "1.3 million articles and summaries written by authors and editors in the newsrooms of 38 major publications. The summaries are obtained from search and social metadata between 1998 and 2017 and use a variety of summarization strategies combining extraction and abstraction"
* [NYTimes Annotated Corpus](https://paperswithcode.com/dataset/new-york-times-annotated-corpus), "over 1.8 million articles written and published by the New York Times between January 1, 1987 and June 19, 2007 with article metadata"

### Audio Data
* [Mozilla CommonVoice](https://commonvoice.mozilla.org/en/datasets), "open source, multi-language dataset of voices that anyone can use to train speech-enabled applications", 2021-07-21, 65 GB, 75,879 voices
* [WikiCommons](https://commons.wikimedia.org/wiki/Category:Audio_files)

## Models

>> "When assessing whether a task is solvable, we first need to ask: should it be solved? And if so, should it be solved by AI?" (Jacobson et al. 2020)

### Language processing
* [DeepSpeech](), Mozilla (created from CommonVoice)
* [Word2Vec]()
* [T5 using C4: Colossal Cleaned Crawled Corpus](https://github.com/google-research/text-to-text-transfer-transformer#datasets) 




## Repositories of datasets

[arXiv.org]()
Started in August 1991, arXiv.org (formerly xxx.lanl.gov) is a highly-automated electronic archive and distribution server for research articles in the areas of physics, mathematics, computer science, nonlinear sciences, quantitative biology and statistics.
[Dataverse]() 
An open source web application to share, preserve, cite, explore, and analyze research data from all different disciplines. Allows individuals to set up their own data repository. Good for satisfying funder requirements for data management.
[Open Access Directory (OAD) Data Repositories] 
A listing of open access data repositories in all subject areas with a concentration on the sciences. Good for identifying data repositories for your own data or finding raw datasets.
[Project Gutenberg Online Catalog]()
Project Gutenberg was the first producer of free electronic books (ebooks).
[re3data(]) - Registry of Research Data Repositories 
A global registry of research data repositories and contents. This registry can be used to search for a data repository or datasets. Good for identifying data repositories for your own data or finding raw datasets.
[PubMed Central]()
Free digital repository of publicly available full-text scholary journal articles in biomedical and life sciences. Articles are free to read without a subscription.
[Data Foundry]()
Data collections from the National Library of Scotland
[Google Datasets Search]()
A search engine for datasets operated by Google
[Harvard Dataverse]()
[Hugging Face Datasets]()
A repository for users to share datasets publicly, operated by Hugging Face
[Kaggle]()
A repository of public datasets operated by Kaggle, which also runs competitions related to machine learning.
[MIT Lincoln Laboratory Datasets]()
A collection of datasets from the MIT Lincoln Laboratory, which researches and develops advanced technologies to meet critical national security needs.
[Papers with Code]()
A resource for machine learning papers, code, datasets, methods and evaluation tables, operated by Meta AI.
[RIsources: Research Infrastructure Portal]()
A portal and catalog operated by the German Research Foundation containing information about scientific research infrastructures which provide researchers with resources and services for planning and implementing research projects. This is a catalog of other catalogs and collections, including datasets.
[UC Irvine Machine Learning Repository]()
The UCI Machine Learning Repository is a collection of databases, domain theories, and data generators that are used by the machine learning community for the empirical analysis of machine learning algorithms.


[^Padilla]: Padilla, T. (2021, October 13). Responsible Operations: Data Science, Machine Learning, and AI in Libraries. OCLC. https://www.oclc.org/research/publications/2019/oclcresearch-responsible-operations-data-science-machine-learning-ai.html
