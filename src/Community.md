# &#128079; Community

## Community values 

> "Care is the practice of savouring how subtle differences can make all the difference." 
>> –*Data Loam*[^Loam]

### Land acknowledgment

This work offers respect and gratitude to the Tongva and Chumash peoples, who are the rightful and traditional caretakers of the land where large portions of this work have been created, along with many other sites. If you have contributed to this project from another location, you are invited to add your own land acknowledgment and visit [Native-Land.ca](https://native-land.ca/) to learn about the history of where you live, why this matters, and how to contribute.


### How we gather

These guidelines are open to inputs and updates as necessary to support the needs of this space as it evolves.

1. scrappy artistic strategies, not perfect code[^ccc]``
2. growth, not perfection[^ccc] 
3. collaboration, not competition[^ccc]
4. we all have skills to teach each other[^ccc]
5. meet people where they are. don't assume knowledge or imply hierarchies of skills. #nocodesnobs #newKidLove[^p5]
6. value processes as much as, or more than, outcomes[^amb]
7. all theory and practice is theory–practice. interdisciplinary goes hand-in-hand with intersectionality[^ccc]
8. #blacklivesmatter #translivesmatter #indigenouslivesmatter engage tension; don't indulge drama[^amb]
9. focus on strategies rather than issues, solutions rather than problems[^AMC]
10. listen from the heart, speak from the heart, be spontaneous, be brief[^Council]
11. assume best intent and attend to impact[^amb]
12. attempt collaboration before conflict[^superrr]
13. accept responsibility for actions and take care to make amends when wrong[^p5]
14. take the lessons, leave the details (confidentiality)[^amb]
15. center care[^AMC] — self care and community care[^amb]

Thank you to the individuals and organizations who developed these principles and protocols: adrienne maree brown,[^amb] Allied Media Conference,[^AMC] Berlin Code of Conduct,[^BerlinCoC] Center for Council,[^Council] P5.js,[^p5] Superrr,[^superrr] W3C,[^W3C] and Creative Code Collective.[^ccc] 

### Events

|Date|Event|
|---|:---|
|19 Aug 2023|[International Summer Academy for Fine Arts Salzburg](https://www.summeracademy.at/kurse/ok-oskar-symposien-kuenstlicher-intelligenz/)|
|16 Aug 2023|[Chaos Communication Camp](https://streaming.media.ccc.de/camp2023/relive/d2b9ad8b-8eae-5781-8aeb-f8246380b29a)|
|1 Jun-9 Jul 2023|[Broken Machines and Wild Imaginings Exhibition, Akademie der Künste](https://aianarchies.net)|
|23 Mar 2023|Mozilla Festival 2023: Unsupervised Pleasures with Emily Martinez|
|27 Jan 2023|[transmediale vorspiel our data bodies workshop](https://vorspiel.berlin/events/our-data-bodies-a-show-program-put-together-by-annika-haas-petja-ivanova)|
|17 Oct 2022|[AI Anarchies Autumn School](https://aianarchies.net/school#day20221017)|
|21 Jul 2022|[ZK/U Open Haus](https://www.zku-berlin.org/timeline/openhaus-july-2022/)|
|11 Jun 2022|[re:publica 22.txt](https://re-publica.com/de/session/making-zines-remaking-machines-intersectional-ai-toolkit-txt-edition)|
|08 Jun 2022|[re:publica 22](https://re-publica.com/de/session/making-zines-remaking-machines-intersectional-ai-toolkit)|
|29 Apr 2022|Coding Care Zine Lab imappening 2022, USC|
|8 Mar 2022|[Mozilla Festival: Making Zines, Remaking Machines](https://schedule.mozillafestival.org/session/3DHCFV-1)|
|14 Nov 2021|Creative Code Collective Zine-Making Workshop, USC|
|13-18 Sep 2021|[POM Berlin](https://www.pomconference.org/pom-berlin-2021-overview/)|
|1 Sep 2021|[HIIG Edit-A-Thon](https://www.hiig.de/en/events/edit-a-thon-intersectional-ai-toolkit/)|
|30 Aug 2021|[MUTEK Forum, Montreal](https://montreal.mutek.org/en/shows/2021/presentations-non-human-explorations-into-algorithmic-creativity)|
|26 Aug 2021|[CARPA7 Uniarts Helsinki](https://sites.uniarts.fi/web/carpa/carpa7)|


<!-- ### Panel discussion from first IAI Edit-a-thon -->

<!-- {{#ev:vimeo\|600258606\|dimensions=640x480\|title=Sarah Ciston in conversation with Nora Al-Badri, Ariana Dongus, Karla Zavarla, and Adriaan Orderaal on 1 Sep 2021}} -->

## Imprint

### Individual contributors

Have you made updates to the IAI Toolkit? Please do add your name here and link to your site. We'd love for everyone to get to know you!

-   [Nora Al-Badri](https://www.nora-al-badri.de/about), edit-a-thon  panelist
-   Hadi Asghari, edit-a-thon participant
-   Anna & Agnieszka & Kian, zine workshop participants
-   Tiffany Conroy, edit-a-thon participant
-   Daniela Dicks, edit-a-thon co-organizer
-   Christina Dinar, edit-a-thon participant and mentor
-   [Ariana Dongus](https://arianadongus.com/), edit-a-thon panelist
-   Miriam Fahimi, edit-a-thon participant
-   Katrin Fritsch, edit-a-thon mentor and participant
-   Geraldine, zine workshop participant
-   [Julie Gough](https://instagram.com/illustratedwih), Illustrated  Women in History, portrait drawing
-   [Johanna Hedva](https://johannahedva.com/), edit-a-thon speaker
-   Freya Hewett, edit-a-thon participant
-   Friederike Kaltheuner, edit-a-thon participant
-   John Lee, zine workshop participant
-   Julia Kloiber, edit-a-thon participant and mentor
-   Julian Klusmann-Rösner, edit-a-thon participant
-   Lucas LaRochelle, edit-a-thon participant
-   Laura Liebig, edit-a-thon participant
-   Siri Lee Lindskrog, zine workshop participant
-   evelyn masso, zine workshop participant
-   Emily Martinez, zine workshops participant
-   Miguel Mercado, zine workshop participant
-   [Leigh Montavon](https://instagram.com/lcmontavon), hand-lettering
-   Eeva Moore, zine workshop participant
-   Katharina Mosene, edit-a-thon participant and mentor
-   [Adriaan Odendaal](https://internetteapot.com/), edit-a-thon  panelist, workshop participant
-   Jason Perez, edit-a-thon participant
-   Carolina Reis, edit-a-thon participant
-   Julia Schneider Gaskin, zine workshop participant
-   Mary Anne Smart, workshop participant
-   Jakob Stolberg-Larsen, edit-a-thon participant
-   Aleksandra Straczek, zine workshop participant
-   Hanna Völkle, edit-a-thon participant and mentor
-   Helene von Schwichow, edit-a-thon participant and mentor
-   Johanna Wallenborn, edit-a-thon organizing support
-   Philipp Weitzel, edit-a-thon organizing support
-   Katrin Werner, edit-a-thon participant and mentor
-   Jessica Wulf, edit-a-thon participant
-   Nushin Yazdani, edit-a-thon participant
-   [Karla Zavala Barreda](https://internetteapot.com/), edit-a-thon  panelist, workshop participant
-   Theresa Züger, edit-a-thon participant
-   Theresa Henne, edit-a-thon participant
-   Katherine Yang, zine workshop participantand magnificent css support

### Project origins 

This toolkit includes the collaboration and support of oh-so-many, and it is a research project started by me, [Sarah Ciston](https://sarahciston.com/) (they/any). 

I'm a poet-programmer who loves building community through creative-critical code. I am white, queer, agender, and from the Ozarks, which is the ancestral home of the Osage and Kickapoo nations. Currently, I'm a PhD Candidate and Annenberg Fellow at [USC's Media Arts and Practice division](https://map.usc.edu/phds/), and an Associated Researcher at the [Humboldt Institute for Internet and Society](https://www.hiig.de/) in Berlin. Recently I have been an AI ethics Fellow at the Akademie der Künste Berlin, a [Mellon PhD Fellow in Digital
Humanities](https://dornsife.usc.edu/digitalhumanities/ph-d-fellows/), and a Google Season of Docs Mentor for Processing Foundation's [P5.js](https://p5js.org/). 

My research investigates how to bring intersectionality to artificial intelligence by employing queer, anti-racist, anti-ableist, neurodiverse, and feminist theories, ethics, and tactics. I also lead [Creative Code Collective](https://creativecodecollective.com/) — a student community for co-learning programming using approachable, interdisciplinary strategies. My creative-critical code-writing projects include an interactive poem of the quantified self and a chatbot that tries to explain feminism to online misogynists. I did my MFA in Literature from UC San Diego and was named one of *San Francisco Weekly*'s "Best Writers Without a Book." 

In *Living a Feminist Life*, Sara Ahmed outlines "A Killjoy Survival Kit," which she says should contain books, things, tools, time, life, permission notes, other killjoys, humor, feelings, bodies, and your own survival kit.[^Ahmed] The Intersectional AI Toolkit is inspired by the kind of cooperative survival she calls for:

>"Protest can be a form of self-care as well as care for others: a refusal not to matter. Self-care can also be those ordinary ways we look out for each other  [...] in queer, feminist, and antiracist work, self-care is about the creation of community, fragile communities  [...] assembled out of the experiences of being shattered. We reassemble ourselves through the ordinary, everyday, and often painstaking work of looking after ourselves; looking after each other."[^Ahmed]

Furthermore, the toolkit I sketch out here is something I want in my own survival kit, and something I hope others will want in theirs, too.

### Contact

For additional input on the project or media inquiries, write to me at <ciston@usc.edu>. You can also reach me on most of the platforms [@sarahciston](https://assemblag.es/@sarahciston) &#x1F49C;

### Personal acknowledgments

Immense credit and thanks go to the communities of Creative Code Collective, Media Arts + Practice, Akademie der Künste, Zentrum fur Kunst und Urbanistic, Humboldt Institute for Internet and Society, Mellon Humanities in a Digital World, HaCCS Lab, Feminist.AI, Chapter510, and P5.js who have taught me how to be in community and have participated in this one. 

### Institutional support

-   Akademie der Künste JUNGE Akademie, [AI Anarchies  fellowship](https://www.adk.de/en/academy/young-academy/ai-anarchies/) 2022, in collaboration with [Zentrum für Kunst und Urbanistik](https://zku-berlin.org)
-   Alexander von Humboldt Institute for Internet and Society, Fellowship, 2021; Associated Researcher 2022–
-   Mellon Foundation, USC Humanities in a Digital World, PhD Fellowship, 2021–2023
-   USC Media Arts + Practice Division, School of Cinematic Arts, Annenberg PhD Fellowship 2017–2022
-   Additional thanks to [MOTIF](https://motif-institute.com/), [netzforma*](https://netzforma.org/), and [SUPERRR](https://superrr.net/)

### Data & privacy

This site does not track or gather any data on its viewers. If you choose to comment on or edit this work, you'll need to create an account with its hosts and thus be agreeing to its host's terms of service. As a viewer, you agree to the host's privacy policies which may include cookies but or share information with third parties. No additional information is collected by Intersectional AI unless explicitly requested and will always be optional.

[^Loam]: Data Loam. 2020. In J. Golding, M. Reinhart, & M. Paganelli (Eds.) De Gruyter. pg 380.

[^ccc]: [Creative Code Collective](https://creativecodecollective.com)  

[^p5]: [p5js](https://p5js.org/community/)  

[^amb]: [brown, am (2017). Emergent Strategies. AK Press.](http://adriennemareebrown.net/) 

[^AMC]:[Allied Media Conference](https://amc.alliedmedia.org/about/purpose-and-values)

[^Council]: [Center for Council](https://www.centerforcouncil.org/)  

[^superrr]: [Superrr](https://superrr.net/code-of-conduct/)

[^BerlinCoC]: [Berlin Code of Conduct](https://berlincodeofconduct.org/)  

[^W3C]: [w3c](https://www.w3.org/)

[^Ahmed]: Ahmed S. *Living a Feminist Life.* 