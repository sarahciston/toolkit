# IAI Futures

<div style="float:left;margin-left:15px;">
<object markdown="1" type="application/pdf" width="700px" height="800px" data="assets/pdf/digital/IAITzine-digital-futures.pdf">[AI Anarchies Autumn School Workshop Intersectional AI Zine PDF](assets/pdf/digital/IAITzine-digital-futures.pdf)</object>

[Print & Fold Edition: A3/Tabloid](assets/pdf/print/IAITzine-a3-futures.pdf)

</div>