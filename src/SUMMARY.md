# Summary

- [&#128391; Start Here](./Start.md)
- [&#128424; Workshop Zines](./Zines.md)
- [&#x1F49C; Love Notes to IAI](./LoveNotes.md)
- [&#127846; Glossary A-to-Z](./Glossaries.md)
- [&#x1F60D; Code Basics](./Code.md)
<!-- - [&#128587; IAI FAQ](./FAQ.md) -->
<!-- - [AI Shouldn't &#128681;](./Shouldnt.md) -->
- [&#128293; Why IAI](./WhyIAI.md)
<!-- - [Intersectionality Means to Me &#129321;](./Intersectionality.md) -->
<!-- - [&#127798; Tactics](./Tactics.md) -->
<!-- - [&#128175; Datasets & Data Bodies](./Datasets.md) -->

<!-- - [IAI Futures &#128302;](./Futures.md) -->
<!-- - [Artists for IAI &#128396;](./Artists.md) -->
<!-- [&#127744; Anarchies] -->

- [&#128079; Community](./Community.md)
- [&#129299; Maker Notes](./Notes.md)