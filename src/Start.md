![logo](assets/img/LC-IAILogo.png)

## &#128213; What is the Intersectional AI Toolkit?

#### A ZINE COLLECTION FOR ARTISTS, ACTIVISTS, MAKERS, ENGINEERS, AND YOU 

The Intersectional AI Toolkit gathers ideas, ethics, and tactics for more ethical, equitable tech. It shows how established queer, antiracist, antiableist, neurodiverse, feminist communities contribute needed perspectives to reshape digital systems. The toolkit also offers approachable guides to both intersectionality and AI. This endeavor works from the hope that code can feel approachable for everyone, can move us toward care and repair---rather than perpetuating power imbalances---and can do so by embodying lessons from intersectionality.

Of course, this toolkit is not the first or only resource on intersectionality or AI. Instead, it gathers together some of the amazing people, ideas, and forces working to re-examine the foundational assumptions built into these technologies. In the tradition of '90s zine aesthetics and politics, it celebrates these radical efforts by sharing them---connecting concepts, creators, tools, and tactics across disciplines and counter-histories---hoping to spark further conversation and collaboration. It does so imperfectly and incrementally, showing rough edges and edit marks, in the belief that no text is final, all code can be forked, and everything is better with friends.

Please join in by exploring the toolkit, commenting with your questions or thoughts, or remixing it into your own new text(s). No experience is necessary to participate; all backgrounds and perspectives are welcome!

## &#128424; Zine collection

Our zine collection grows with every in-person and online zine-making workshop: 

{{#include Zines.md:zinelist}}

These online issues are continual works-in-progress: READABLE, REMIXABLE, SHAREABLE. 

* &#127846;&#127846; [A-to-Z IAI & FAQ](./Glossaries.md) A double-sided glossary from AI technical perspectives and social perspectives
*  &#x1F49C; [Who Loves IAI](./LoveNotes.md) Great practitioners and projects already at work
*  &#128293; [Why We Need IAI](./WhyIAI.md) Making the case for more equitable, empathic tech
*  &#127798; [Tactics for IAI](./Tactics.md) Practical approaches from wide-ranging communities and decades of intersectional effort
*  &#x1F60D; [Help Me Code IAI](./Code.md) Afraid of programming but want to save AI from itself?
*  &#129321; [Intersectionality Means to Me](Intersectionality.md) Why listen to academic, activist, or artistic approaches?

[Full Toolkit PDF Render](https://gitlab.com/api/v4/projects/sarahciston/toolkit/jobs/artifacts/main/raw/output/intersectionalAI.pdf?job=pdf)

{{#include Zines.md:makezine}}

#### ... and it's pronounced zeen, right?

Yep, like "magazine." Like, "I'm so excited to read this zeeeeeeen!"

This wiki/zine library of digital-print hybrid zines are written for a non-academic/non-technical audiences and are intended as practical introductory field guides to key concepts, strategies, and resources around inclusive, intersectional AI. But mainly they are intended as jumping off points for your own practice, inspiration, and continued conversation. They celebrate, cite, remix, reframe, and you should feel welcome to do the same.

Christina Dunbar-Hester, in Hacking Diversity (2020), notes that the perhaps surprising appearance of zines and crafting in feminist technology circles makes sense: "In feminist zine making, forms of knowledge like folk medicine can be filtered through the riot grrrl practice of zine-making, which is itself connected to long traditions of feminine papercraft and journaling. They are identity practices in addition to circulations of knowledge" (111). As digital-print hybrids, they can utilize the liveliest aspects of both: GIFS and images, the expandability and nonlinearity of hyperlinks, and other dynamic content online; but also the accessible, 'handmade', distributable, low-power novelty of paper at a moment of maximum screen fatigue. The print versions of these zines are formatted to print double-sided on a single landscape page, making reversible two-part mini-zines (in multiple combinations as the library grows).

They are also inspired by the [Tiny Tech Zines](https://tinytechzines.org/) festival and two fantastic zines
acquired there ["Bite-Size Networking"]() by Julia Evans and ["How to Cite Like a Badass Feminist Tech Scholar of Color"]() by Data and Society's Rigoberto Lara Guzmán and Sareeta Amrute. Check out the **[Who Loves IAI](./LoveNotes.md)** zine for other zines we love, like the [Techno-Galactic Guide to Software Observation]().

## &#129299; Who's making this IAI TK?

Developed by Sarah Ciston while a virtual fellow at the HIIG, with valued inspiration and collaboration from many others included on the **[Community](./Community.md)** page. See **[Process Notes](./Notes.md)** for more on Sarah's making of this collection-in-progress and read more below about how you can help it grow.

## &#129303; ...and how can I contribute?

#### Comment, edit, & remix this 

There are many ways to contribute. One of the most immediate is to join in the co-creation of these texts! Feel free to read along, and add your thoughts on the project's [archive](https://github.com/sarahciston/intersectionalai). I think of this as an expanded form of reading-writing. 

At the top of any page on this site, you'll see links to "Suggest an edit" and "Git repository." When you follow these, you're invited to make a user account to make edits and suggestions to any page. You can also look at all the prior versions and compare any old version to any new one. Don't worry, you can't break anything! You can even copy this entire repository (archive) as a model to make your own digital zine too.

#### Add your thoughts, edits, and new pages in style 

How do I format my contribution? 

* [Handy Markdown](https://devhints.io/markdown)
* [Official Markdown](https://kramdown.gettalong.org/quickref.html) 
* [Printable Markdown cheatsheet](https://packetlife.net/media/library/16/Markdown.pdf)

## &#129302; See also 

#### Creative Code Collective Resource Hub 

The [Creative Code Collective Resource Hub](https://creativecodecollective.com/resource-hub.html) is an ever-expanding, community-sourced database of inspiring projects, tutorials, and coding tools — curated for critical and creative learning. The IAI Toolkit is closely partnered with the Resource Hub, which has been curated by members of Creative Code Collective and friends. It is an interactive, searchable, sortable database that will point you to the many different kinds of projects, tools, and research being made around creative coding, Intersectional AI, and related topics. It's fed by a user-friendly spreadsheet where you can add your own resources and tell us why you like them. Get inspired and get cracking making your own intersectional projects and tools for others.

#### Trans*formative TechnoCraft & Coding.Care

Elsewhere Sarah is working on documenting the process of building the Intersectional AI Toolkit and Creative Code Collective as an experimental dissertation. *[Trans*formative TechnoCraft](https://coding.care)* includes how-to guides for creating and sustaining communities around critical-creative code, working critically with machine learning datasets, and intersectionally with AI systems. 